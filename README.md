# Formale Semantik Project: Word Sense Induction

##### Done by Maria Stepanov

The packages that need to be installed can be found in the file
requirement.txt.
From nltk, WordNet needs to be downloaded (only used for Lemmatization)

To run the project like it is, just run the file wsi.py.
To run the project on another dataset, then paste the files in a folder, in the same directory
as wsi.py. You have to change the lines 20 and 27 to the right path
of the file, so the script will be able to read in the files.

After running the script, an output file will be generated in the same
directory as the script, called "output.txt".
This file already exists and is the latest output that the script produced.
So if you run the system in your own dataset, the output file will be overwritten.

A copy of the latest produced output file exists under the name "output_test.txt"
It's just a copy of the output file that was returned by the script that ran over
the test dataset given at the end of the project. It was required that this file was also
handed in, so it exists twice, just in case. Back-up is always a good idea.

**Thanks!**
