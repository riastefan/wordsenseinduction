from __future__ import division
from gensim.models import Doc2Vec
from gensim.models.doc2vec import LabeledSentence
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk import cluster
from nltk.cluster import KMeansClusterer
from string import punctuation
import csv
from sklearn.cluster import KMeans
from sklearn import metrics
from sklearn.decomposition import PCA
import pylab as pl
import matplotlib.pyplot as plt
import sys, codecs, numpy
import nltk
from nltk.stem.wordnet import WordNetLemmatizer

Topics = [] #List in list, [[TopicId, Description],..]
with open("./semeval-2013_task11_test/test/topics.txt", encoding="utf8") as csvfile:
    readCSV = csv.DictReader(csvfile, delimiter='\t')
    for row in readCSV:
        ID = row["id"]
        Topics.append([ID, row["description"]])

Results = [] # List in List, [[TopicId, SearchEngineRandId(of respective Topic), URL, title, Snippet],...
with open("./semeval-2013_task11_test/test/results.txt", encoding="utf8") as csvfile:
    readCSV = csv.DictReader(csvfile, delimiter='\t')
    for row in readCSV:
        ID, resID = row["ID"].split(".")
        Results.append([ID, resID, row["url"], row["title"], row["snippet"]])


#now we need to tokenize everything -> in that case the snippets. We're just gonna save the tokenized snippets where they
#are right now, -> we're gonna just overwrite everything
#tokenize, stem, remove punctuation etc.

#we also want to remove "useless" words that don't contribute to the context of a snippet
#like this, IMO the word embeddings will be more truthful to their actual context
uselesswords =["the", "of", "a", "an", "I", "you", "and" ,"or", "in", "as", "of", "out" ,"for", "that","'s", "it", "to","into" ,"is" ,"was" ,"have", "has", "by", "with", "on", "``","´´","#"]
lmtzr = WordNetLemmatizer()

for item in Results:
    item[4] = word_tokenize(str(item[4]))
    item[4] = [token.lower() for token in item[4]]
    item[4] = [token for token in item[4] if token not in uselesswords]
    item[4] = [token for token in item[4] if token not in list(punctuation) + ["..."]]
    postags = nltk.pos_tag((item[4]))
    for token in item[4]:
        token = token.lower()
        token = PorterStemmer().stem(token)
        token = lmtzr.lemmatize(token)
    for i in range(len(postags)):
        item[4][i] = item[4][i] + "|" + postags[i][1]

#print(len(Results))

#Word2Vec Model
#sentences = [item[4] for item in Results]
#print(len(sentences))
#model = Word2Vec(sentences, size=100, window=20, min_count=1, workers=cpu_count(), iter=200, sg=1)
#model.train(sentences, total_examples=model.corpus_count, epochs=model.iter)

#Doc2Vec Model
class LabeledLineSentence:
    def __init__(self, sents):
        self.Snippets = sents
    def __iter__(self):
        for item in self.Snippets:
            yield LabeledSentence(words = item[4], tags = [item[0]+"."+item[1]])

sentences = []
for item in Results:
    sentences.append([item[4]]) #label = ResultId, was separated when writing to the list
labels = []
for item in Results:
    labels.extend(item[0]+"."+item[1])

it = LabeledLineSentence(Results)
count = len(Results)
model = Doc2Vec(alpha=0.025, min_alpha=0.025, epochs = 1)  # use fixed learning rate, "epochs= 1 because we keep training ourselves" (iter is deprecated)
model.build_vocab(it)
for epoch in range(10):
    model.train(it, total_examples=count, epochs=1)
    model.alpha -= 0.002  # decrease the learning rate
    model.min_alpha = model.alpha  # fix the learning rate, no decay


#len(model.docvecs = len(Results) - we now have the same amount of vectors as we have snippets. Now we could try clustering
#all of them
nclusters = len(Topics)*6
kmeans_model = KMeans(n_clusters=nclusters, init='k-means++', max_iter=200)
X = kmeans_model.fit(model.docvecs.doctag_syn0)
labels=kmeans_model.labels_.tolist()

l = kmeans_model.fit_predict(model.docvecs.doctag_syn0)
pca = PCA(n_components=2).fit(model.docvecs.doctag_syn0)
datapoint = pca.transform(model.docvecs.doctag_syn0)

"""
kclusterer = KMeansClusterer(200, distance=nltk.cluster.util.cosine_distance, repeats=25)
assigned_clusters = kclusterer.cluster(model.docvecs, assign_clusters=True)
print(assigned_clusters)
"""
#aaaaand now we plot so we see our results. yay! (don't use it now since we have like, 20 cluster nows and I didn't put enough
#colours in there
"""
plt.figure
label1 = ["#FFFF00", "#028030", "#0000FF", "#805080","#FBB0FFF0", "#658325A1", "#101BBB10", "#C5C5F5F5", "#A4A4A4A4", "#BBBBBB", "#2B4C6F", "#123FFF", "#AAA123", "#222BBB", "#BFB121", "#212CBC", "#CCCCCC", "#AAAAA1", "#BBBCCC", "#CCCCCC", "#C3C2C1", "#B2B1B3", "#FDDFDF", "#DDDDD1"]
color = [label1[i] for i in labels]
plt.scatter(datapoint[:, 0], datapoint[:, 1], c=color)

centroids = kmeans_model.cluster_centers_
centroidpoint = pca.transform(centroids)
plt.scatter(centroidpoint[:, 0], centroidpoint[:, 1], marker='^', s=150, c='#000000')
plt.show()
"""

#writing the clustering results into output.txt

with open("output.txt", "w", newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter='\t')
    writer.writerow(["subTopicID", "resultID"])
    for i in range(len(Results)):
       subid = "%s" % (Results[i][0]+"."+str(labels[i]))
       resid = "%s" % (Results[i][0]+"."+Results[i][1])
       writer.writerow([subid, resid])







